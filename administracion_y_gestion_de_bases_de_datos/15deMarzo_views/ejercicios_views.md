


### Administración y Gestión de Bases de Datos

---
#### Views / Vistas

---

- [Material utilizado · Documentación Oficial de MySQL · Views](https://dev.mysql.com/doc/refman/8.0/en/view-syntax.html)
---

#### Guía de ejercicios

-  Dado el modelo de datos llamado classicmodels, importar la base de datos proporcionada
  (base_de_datos_classicmodels.sql) y luego definir las siguientes vistas.
  
1. Crear una vista DetalleOrden que muestre para todas las ordenes, los códigos de producto que la
componen, indicando del producto: el código, nombre, descripción, cantidad ordenada, precio unitario
y el precio total por producto.
2. Crear una vista totalPorOrden que muestre el total por orden.
3. Crear una vista que liste todos los productos que tengan precio mayor al precio promedio.
4. Crea una vista que liste todos los productos con precio menor al precio promedio.
5. Cree una vista que liste para todas las oficinas, el codigo de oficina y la ciudad junto con el código de
empleado y nombre y apellido de los mismos, ordenado por código de oficina, código de empleado.
6. Cree una vista que refleje todos los clientes que aun no hayan registrado pagos.

7. Cree una vista que liste todas las ordenes correspondientes a todos los clientes.

8. Cree una vista que liste para cada cliente: el numero de cliente, el nombre del cliente y la fecha de orden de todas las ordenes del cliente.

9. Cree una vista que muestre la cantidad de productos que se cuentan por cada linea de producto.

10. Cree una vista que liste los siguientes datos para todas las oficinas: el codigo de oficina y el apellido de los empleados que pertecen a la misma. 
