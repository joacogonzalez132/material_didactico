----
#### Algoritmos y Estructuras de Datos · 21 de Marzo
----
##### Aclaraciones con respecto a los ejercicios del día jueves 14 de Marzo.

- Resolver los siguientes problemas propuestos en el libro de algoritmos utilizado la clase pasada. En cada punto,
deberán proponer el pseudocódigo y el diagrama de ﬂujo correspondiente.

  - Ejercicios con estructuras secuenciales:

    - 2.9. Para realizar este ejercicio, se deberán recibir los siguientes datos por consola: la cantidad de minutos de la llamada y el costo de cada minuto. Mostrar el resultado al finalizar el cálculo.
    
    - 2.12. Para realizar este ejercicio, se deberán recibir los siguientes datos por consola: el precio del artículo. Mostrar el resultado al finalizar los cálculos. Opcional: mostrar los precios parciales, es decir, el precio del descuento y luego el precio con el valor de IVA correspondiente.
  
    - 2.13. Para realizar este ejercicio, se deberá recibir el sueldo mensual de la persona. Mostrar el resultado al finalizar los cálculos.

  - Ejercicios con estructuras de selección:

    - 3.1. Para realizar este ejercicio, se deberá recibir la edad del usuario por consola. No realizar el diagrama N/S.
    
    - 3.2. Para realizar este ejercicio, se deberán recibir los siguientes datos por consola: la cantidad de horas trabajadas y el valor de pago correspondiente a una hora trabajada. No realizar el diagrama N/S.
    
    - 3.4. Para realizar este ejercicio, se deberá recibir por cosola la cantidad de horas que el cliente estuvo estacionado. 
    
    - 3.14. Para realizar este ejercicio, se deberá recibir el siguiente dato por consola: una calificación, es decir, un número del 1 al 10, inclusive. Proponer dos soluciones: una solución utilizando la estructura condicional simple (if) y una solución utilizando la estructura condicional múltiple (switch-case). No realizar el diagrama N/S.
    
    - 3.15. Para realizar este ejercicio, se deberá recibir el siguiente dato por consola: un número del 1 al 7, inclusive. Proponer dos soluciones: una solución utilizando la estructura condicional simple (if) y una solución utilizando la estructura condicional múltiple (switch-case). No realizar el diagrama N/S.
    
    - 3.19. Para realizar este ejercicio, se deberá recibir el siguiente dato por consola: la edad de la persona. Al finalizar, indicar mediante un mensaje cuál es la vacuna de debe aplicarse la persona.
    
    

- Resolver los siguientes problemas utilizando la estructura repetitiva vista. En cada punto, deberán proponer el
pseudocódigo y el diagrama de ﬂujo correspondiente.

  - a. Mostrar los números desde el 1 al 1000.
  - b. Sumar los primeros 100 números y luego mostrar el resultado de la suma.
  - c. Mostrar los números desde el 100 al 5000.
  - d. Pedirle al usuario una cantidad indeterminada de números y sumarlos. En caso de que el usuario ingrese el 0,
  ya no se deberá pedir números y luego se deberá mostrar el resultado de la suma.
  - e. Pedirle al usuario un número mayor o igual a 1 y luego mostrar todos los números positivos que son menores o
  iguales al número recibido. Ejemplo: si el usuario ingresa el número 3, se deberán mostrar los números 1, 2 y 3.
